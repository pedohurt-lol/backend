const axios = require("axios");
const crypto = require("crypto");
const fs = require("fs").promises;

let data;
fs.readFile("/data/UpdatedDB.json").then((d) => 
    data = JSON.parse(d)
).catch((err) => 
    console.log)

const hash = (data) => crypto.createHash("sha512").update(`${process.env.PASSWORD_SALT_PREFIX}${data}${process.env.PASSWORD_SALT_SUFFIX}`).digest("hex");
module.exports = {
    validateCaptcha: (recaptchaResponse) => new Promise((resolve, reject) => 
        axios.get(
            `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RC_SECRET_KEY}&response=${recaptchaResponse}`
        )
        .then((response) => 
            response.data.success ? resolve() : reject())
        .catch((err) => 
            reject()
        )
    ),
    checkEmail: (email) => new Promise(async (resolve, reject) => {
        return data[email] ? resolve(data[email]) : reject();
    }),
    checkPassword: (password) => new Promise(async (resolve, reject) => {
        Object.keys(data).forEach(key => {
            const object = data[key];
            if(hash(password) == object.password) return resolve();
        });
        reject();
    })
}
